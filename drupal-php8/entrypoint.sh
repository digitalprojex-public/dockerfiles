#!/bin/sh

set -e
echo "==>> Configure php.ini"

PHP_FPM_CONF=/etc/php81/php-fpm.d/www.conf
echo "max_execution_time = ${PHP_MAX_EXECUTION_TIME}" >> $PHP_FPM_CONF
echo "memory_limit = ${PHP_MEMORY_LIMIT}" >> $PHP_FPM_CONF
echo "display_errors = ${PHP_DISPLAY_ERRORS}" >> $PHP_FPM_CONF
echo "post_max_size = ${PHP_POST_MAX_SIZE}" >> $PHP_FPM_CONF
echo "upload_max_filesize = ${PHP_UPLOAD_MAX_FILESIZE}" >> $PHP_FPM_CONF

PHP_INI=/etc/php81/php.ini
sed -e 's/max_execution_time = 30/max_execution_time = '${PHP_MAX_EXECUTION_TIME}'/' -i $PHP_INI
sed -e 's/memory_limit = 128M/memory_limit = '${PHP_MEMORY_LIMIT}'/' -i $PHP_INI
sed -e 's/display_errors = Off/display_errors = '${PHP_DISPLAY_ERRORS}'/' -i $PHP_INI
sed -e 's/post_max_size = 8M/post_max_size = '${PHP_POST_MAX_SIZE}'/' -i $PHP_INI
sed -e 's/upload_max_filesize = 2M/upload_max_filesize = '${PHP_UPLOAD_MAX_FILESIZE}'/' -i $PHP_INI

echo "==>> Configure nginx"

if [ -z "$ROOT_DIR" ];then
  ROOT_DIR=${APP_DIR}/web
fi

if [ -z "$SERVER_NAME" ];then
  SERVER_NAME='drupal.org'
fi

# Set server root
sed -i "s#ENV_SERVER_NAME#${SERVER_NAME}#" /etc/nginx/sites-enabled/*.conf
sed -i "s#ENV_ROOT_DIR#${ROOT_DIR}#" /etc/nginx/sites-enabled/*.conf

echo "** Running **"
supervisord --nodaemon --configuration /etc/supervisord.conf
