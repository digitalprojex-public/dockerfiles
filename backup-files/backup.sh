#!/bin/sh

source backup-core.sh

SOURCE_DIRECTORY="/var/files"

echo ======================================================================
echo AutoBackupFiles
echo ======================================================================

echo Backup Start Time `date`
echo ======================================================================
# Monthly Full Backup of all Directorys
cd $SOURCE_DIRECTORY
DIR_LIST=$(ls -d */)

if [[ $(echo $FRECUENCY | grep -c monthly) == 1 ]] && [[ $DOM = "01" ]]; then
    for i in $DIR_LIST; do
    _dir=${i%%/};

   M_DIR="$BACKUPDIR/$_dir/monthly"
   if [[ ! -e "${M_DIR}" ]]; then		# Check Monthly Directory exists.
        mkdir -p "${M_DIR}"
   fi

    echo Monthly Backup of $_dir...
    compression_dest=""${M_DIR}"/${_dir}-month-$DATE.tar.gz"
    compression_folder $_dir $compression_dest

    echo Delete weekly backups
    rotate "${M_DIR}" "${RETAIN_MONTHLY}"

    echo ----------------------------------------------------------------------
    done
fi

for i in $DIR_LIST; do
    _dir=${i%%/};

    # Weekly Backup
    if [[ $(echo $FRECUENCY | grep -c weekly) == 1 ]] && [[ $DNOW = $DOWEEKLY ]]; then
        echo Weekly Backup of Directory \( $_dir \)
        echo

        W_DIR="$BACKUPDIR/$_dir/weekly"
        if [[ ! -e "${W_DIR}" ]]; then		# Check Weekly Directory exists.
            mkdir -p "${W_DIR}"
        fi
        compression_dest="${W_DIR}/${_dir}-week-$DATE.tar.gz"
        compression_folder $_dir $compression_dest

        echo Delete daily backups
        rotate "${W_DIR}" "${RETAIN_WEEKLY}"
        echo ----------------------------------------------------------------------

    # Daily Backup
    else
        if [[ $(echo $FRECUENCY | grep -c daily) == 1 ]]; then
            echo Daily Backup of Directory \( $_dir \)
            echo

            D_DIR="$BACKUPDIR/$_dir/daily"
            # Create Separate directory for each DIR
            if [[ ! -e "${D_DIR}" ]]; then		# Check Daily Directory exists.
                mkdir -p "${D_DIR}"
            fi

            compression_dest=""${D_DIR}"/${_dir}-$DATE.tar.gz"
            compression_folder $_dir $compression_dest
            rotate "${D_DIR}" "${RETAIN_DAILY}"
           echo ----------------------------------------------------------------------
        fi
    fi
done
echo Backup End `date`
echo ======================================================================
echo
echo

exit 0
