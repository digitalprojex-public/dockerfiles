#!/bin/sh
set -e

if $NPM_INSTALL; then
    npm install $NPM_PACKAGES --unsafe
fi

if $NPM_LINK; then
  npm link $NPM_PACKAGES
fi

exec "$@"
