#!/bin/sh

echo "==>> Configure nginx"
# Ensure server APP_DIR defined.
if [ -z "$APP_DIR" ]; then
    APP_DIR=/var/app
fi

ROOT_WEB_DIR=${APP_DIR}/web

# Set server root
sed -i "s#ROOT_WEB_DIR#${ROOT_WEB_DIR}#" /etc/nginx/sites-enabled/*.conf

echo "=> Create /var/log/nginx"
mkdir -p /var/log/nginx

echo "=> Create /var/log/app"
mkdir -p /var/log/app
chmod -R 777 /var/log/app

echo "==>> Install composer dependencies"
if $COMPOSER_INSTALL; then
    rm -f composer.lock
    composer install
    composer dump-autoload --optimize
fi

echo "** Running **"
supervisord --nodaemon --configuration /etc/supervisord.conf
