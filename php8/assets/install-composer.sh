#!/bin/sh

echo
echo
echo "==>> Install Composer"
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer
echo "PATH=$PATH:~/.composer/vendor/bin" >> ~/.bash_profile
source ~/.bash_profile
echo "==>> Finish Install Composer <<=="
echo
echo
