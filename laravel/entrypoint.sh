#!/bin/sh

echo "=> Create /var/log/nginx"
mkdir -p /var/log/nginx

STORAGE_PATH=/var/www/html/storage
mkdir -p ${STORAGE_PATH}/logs
mkdir -p ${STORAGE_PATH}/framework/cache/data
mkdir -p ${STORAGE_PATH}/framework/sessions
mkdir -p ${STORAGE_PATH}/framework/testing
mkdir -p ${STORAGE_PATH}/framework/views
chmod -R 775 ${STORAGE_PATH}
chown -R nobody:nobody ${STORAGE_PATH}

echo "==> Exec storage:link <=="
php artisan storage:link

echo "** Running **"
supervisord --nodaemon --configuration /etc/supervisord.conf
