#!/bin/sh

BACKUPDIR="${BACKUPDIR_BASE}/${BACKUP_TYPE}"

# ============================================================
# === ADVANCED OPTIONS ( Read the doc's below for details )===
#=============================================================

# Which day do you want weekly backups? (1 to 7 where 1 is Monday)
DOWEEKLY=7

DATE=`date +%Y-%m-%d`				# Datestamp e.g 2002-09-21-23-45-11
HOUR=`date +%H-%M-%S`				# Hourstamp e.g. 23-50-23
DOW=`date +%A`					# Day of the week e.g. Monday
DNOW=`date +%u`					# Day number of the week 1 to 7 where 1 represents Monday
DOM=`date +%d`					# Date of the Month e.g. 27
M=`date +%B`					# Month e.g January
W=`date +%V`					# Week Number e.g 37
OPT=""						# OPT string for use with mysqldump ( see man mysqldump )

# Choose Compression type. (gzip or bzip2)
COMP=bzip2

# Create required directories
if [[ ! -e "$BACKUPDIR" ]]; then		# Check Backup Directory exists.
	mkdir -p "$BACKUPDIR"
fi

# Functions

# Compression folder function
compression_folder () {
    tar -zcf $2 $1
    return 0
}

# Compression function
SUFIX=""
compression_file () {
  if [[ "$COMP" = "gzip" ]]; then
    gzip -f "$1"
    echo
    echo Backup Information for "$1"
    gzip -l "$1.gz"
    SUFIX=".gz"
  elif [[ "$COMP" = "bzip2" ]]; then
    echo Compression information for "$1.bz2"
    bzip2 -z $1
    SUFIX=".bz2"
  else
    echo "No compression option set, check advanced settings"
  fi
  return 0
}

# Delete the oldest files depending on the retention parameter
rotate () {
    FILES_DIR=$1
    RETAIN=$2
    _ROOT_DIR=$(pwd)
    cd ${FILES_DIR}
    RETAIN=$((RETAIN+1))
    if [[ "$(ls -t | tail -n +${RETAIN})" != "" ]]; then
        ls -t | tail -n +${RETAIN} | xargs rm -f --
    fi
    cd ${_ROOT_DIR}
}


