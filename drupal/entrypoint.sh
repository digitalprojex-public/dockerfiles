#!/bin/sh

set -e
echo Configure php.ini

echo "max_execution_time = ${PHP_MAX_EXECUTION_TIME}" >> /etc/php7/php-fpm.d/www.conf
echo "memory_limit = ${PHP_MEMORY_LIMIT}" >> /etc/php7/php-fpm.d/www.conf
echo "display_errors = ${PHP_DISPLAY_ERRORS}" >> /etc/php7/php-fpm.d/www.conf
echo "post_max_size = ${PHP_POST_MAX_SIZE}" >> /etc/php7/php-fpm.d/www.conf
echo "upload_max_filesize = ${PHP_UPLOAD_MAX_FILESIZE}" >> /etc/php7/php-fpm.d/www.conf

sed -e 's/max_execution_time = 30/max_execution_time = '${PHP_MAX_EXECUTION_TIME}'/' -i /etc/php7/php.ini
sed -e 's/memory_limit = 128M/memory_limit = '${PHP_MEMORY_LIMIT}'/' -i /etc/php7/php.ini
sed -e 's/display_errors = Off/display_errors = '${PHP_DISPLAY_ERRORS}'/' -i /etc/php7/php.ini
sed -e 's/post_max_size = 8M/post_max_size = '${PHP_POST_MAX_SIZE}'/' -i /etc/php7/php.ini
sed -e 's/upload_max_filesize = 2M/upload_max_filesize = '${PHP_UPLOAD_MAX_FILESIZE}'/' -i /etc/php7/php.ini

echo "==>> Configure nginx"

if [ -z "$ROOT_DIR" ];then
  ROOT_DIR=${APP_DIR}/web
fi

if [ -z "$SERVER_NAME" ];then
  SERVER_NAME='drupal.org'
fi

# Set server root
sed -i "s#ENV_SERVER_NAME#${SERVER_NAME}#" /etc/nginx/sites-enabled/*.conf
sed -i "s#ENV_ROOT_DIR#${ROOT_DIR}#" /etc/nginx/sites-enabled/*.conf

echo "** Running **"
supervisord --nodaemon --configuration /etc/supervisord.conf
