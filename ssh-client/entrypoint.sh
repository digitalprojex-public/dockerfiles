#!/bin/sh

# Para que se ejecute una sola ves
if [[ ! -f /ci_runned ]] ; then
    export SSH_EXEC="ssh -tt ${DEPLOY_HOST}"
    echo 'Prepare SSH'
    eval $(ssh-agent -s)
    bash -c 'ssh-add <(echo "$SSH_PRIVATE_KEY")'
    mkdir -p ~/.ssh
    if [[ -f /.dockerenv ]]; then
        echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
    fi
fi

[[ $CI ]] && touch /ci_runned

/bin/sh
