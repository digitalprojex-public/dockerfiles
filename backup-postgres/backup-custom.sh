#!/bin/sh

source backup-core.sh
source vars.sh

DB=$1
if [ ! ${DB} ]; then
    echo "Database name not pass as argument!!! "
    exit 1
fi

echo Backup Start for ${DB}-${DATE}-${HOUR}.sql${SUFIX}
echo ======================================================================

C_DIR="$BACKUPDIR/${DB}/custom"

_file_name=${C_DIR}/${DB}-${DATE}-${HOUR}.sql
mkdir -p "${C_DIR}"
dbdump "$DB" "${_file_name}"
compression_file "${_file_name}"

rotate "${C_DIR}" "${RETAIN_CUSTOM}"

echo Backup End for ${DB}-${DATE}-${HOUR}.sql${SUFIX}
echo ======================================================================
echo
echo

exit 0
