#!/bin/sh

source backup-core.sh
source vars.sh

echo ======================================================================
echo Backup of Database Server - $DBHOST
echo ======================================================================

echo Backup Start Time `date`
echo ======================================================================
# Monthly Full Backup of all Databases
if [[ $DOM = "01" ]]; then
    for MDB in $MDBNAMES; do
        # Prepare $DB for using
        MDB="`echo $MDB | sed 's/%/ /g'`"

        M_DIR="$BACKUPDIR/$MDB/monthly"
        if [[ ! -e "${M_DIR}" ]]; then		# Check Monthly Directory exists.
            mkdir -p "${M_DIR}"
        fi

        echo Monthly Backup of $MDB...
        _file_name=${M_DIR}/${MDB}-month-$DATE.sql
        dbdump "$MDB" "${_file_name}"
        compression_file "${_file_name}"
        rm -f ${_file_name}

        echo Delete weekly backups
        rotate "${M_DIR}" "${RETAIN_MONTHLY}"
        echo ----------------------------------------------------------------------
    done
fi

for DB in $DBNAMES; do
  # Prepare $DB for using
  DB="`echo $DB | sed 's/%/ /g'`"

  # Weekly Backup
  if [[ $DNOW = $DOWEEKLY ]]; then
      echo Weekly Backup of Database \( $DB \)
      echo

      W_DIR="$BACKUPDIR/${DB}/weekly"
      if [[ ! -e "${W_DIR}" ]]; then		# Check Weekly Directory exists.
        mkdir -p "${W_DIR}"
      fi

      _file_name=${W_DIR}/${DB}-week-$DATE.sql
      dbdump "$DB" "${_file_name}"
      compression_file "${_file_name}"
      rm -f ${_file_name}

      echo Delete weekly backups
      rotate "${W_DIR}" "${RETAIN_WEEKLY}"
      echo ----------------------------------------------------------------------

  # Daily Backup
  else
      echo Daily Backup of Database \( $DB \)
      echo

      D_DIR="$BACKUPDIR/${DB}/daily"
      if [[ ! -e "${D_DIR}" ]]; then		# Check Daily Directory exists.
        mkdir -p "${D_DIR}"
      fi

      _file_name=${D_DIR}/${DB}-$DATE.sql
      dbdump "$DB" "${_file_name}"
      compression_file "${_file_name}"
      rm -f ${_file_name}

      echo Delete daily backups
      rotate "${D_DIR}" "${RETAIN_DAILY}"
      echo ----------------------------------------------------------------------
  fi
done

echo Backup End `date`
echo ======================================================================
echo
echo

exit 0
