# Username to access the PostgreSQL server e.g. dbuser
# DBUSER=root

# Password
# create a file $HOME/.pgpass containing a line like this
#   hostname:*:*:dbuser:dbpass
# replace hostname with the value of DBHOST and postgres with
# the value of USERNAME

# Host name (or IP address) of PostgreSQL server e.g localhost
# DBHOST=postgres

# List of DBNAMES for Daily/Weekly Backup e.g. "DB1 DB2 DB3"
#DBNAMES="all"


# ============================================================
# === ADVANCED OPTIONS ( Read the doc's below for details )===
#=============================================================

# List of DBBNAMES for Monthly Backups.
MDBNAMES="$DBNAMES"

# List of DBNAMES to EXLUCDE if DBNAMES are set to all (must be in " quotes)
#DBEXCLUDE="template1 postgres $DBS_EXCLUDE"

# Include CREATE DATABASE in backup?
CREATE_DATABASE=yes

PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/postgres/bin:/usr/local/pgsql/bin

if [ "$CREATE_DATABASE" = "no" ]; then
    OPT="$OPT"
else
    OPT="$OPT --create"
fi

# Hostname for LOG information
if [ "$DBHOST" = "localhost" ]; then
	DBHOST="`hostname -f`"
	HOST=""
else
	HOST="-h $DBHOST"
fi


# If backing up all DBs on the server
if [ "$DBNAMES" = "all" ]; then
     DBNAMES="`psql -U $DBUSER $HOST -l -A -F: | sed -ne "/:/ { /Name:Owner/d; /template0/d; s/:.*$//; p }"`"

	# If DBs are excluded
	for exclude in $DBEXCLUDE; do
		DBNAMES=`echo $DBNAMES | sed "s/\b$exclude\b//g"`
	done
    MDBNAMES=$DBNAMES
fi


# Functions
# Database dump function
dbdump () {
    pg_dump --username=$DBUSER $HOST $OPT $1 > $2
    return 0
}
