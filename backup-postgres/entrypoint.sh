#!/bin/sh

set -e
echo "==>> Prepare .pgpass"
echo "${DBHOST}:*:*:${DBUSER}:${DBPASS}" > /root/.pgpass
chmod 600 /root/.pgpass

/backup.sh
