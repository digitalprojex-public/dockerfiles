docker login registry.gitlab.com -u gitlab-ci-token -p $CI_JOB_TOKEN

DOCKER_REGISTRY_BASE=registry.gitlab.com/digitalprojex-public/dockerfiles

#images=$(ls -d */)
images="laravel laravel-node"

touch images_build

ROOT_DIR=$(pwd)
for image in $images; do
    if [[ -d "${image}" ]] && [[ -f "${image}/build.sh" ]]; then
        echo ".."
        echo "                ====>>  Build ${image}  <<===="
        echo "................................................"
        echo "................................................"
        cd ${image}
        source ./build.sh

        IMAGE_TAG_VERSION=${DOCKER_REGISTRY_BASE}/${IMAGE_TAG_NAME}:${IMAGE_TAG}
        docker build -t ${IMAGE_TAG_VERSION} ${ARG} . || { echo "Build ${IMAGE_TAG_NAME}:${IMAGE_TAG} filed" ; exit 1; }
        docker push ${IMAGE_TAG_VERSION} || { echo "Push  ${IMAGE_TAG_VERSION} filed" ; exit 1; }

        cd ${ROOT_DIR}
        echo "${IMAGE_TAG_VERSION}" >> "images_build"

        if [[ "$IMAGE_TAG" != "latest" ]]; then
            IMAGE_TAG_LATEST=${DOCKER_REGISTRY_BASE}/${IMAGE_TAG_NAME}:latest
            docker tag ${IMAGE_TAG_VERSION} ${IMAGE_TAG_LATEST}
            docker push ${IMAGE_TAG_LATEST} || { echo "Push  ${IMAGE_TAG_VERSION} filed" ; exit 1; }
            echo "${IMAGE_TAG_LATEST}" >> "images_build"
        fi
    fi
done

echo "..................................................................................."
echo "..................................................................................."
echo "..................................................................................."
echo "     ====>>>>    Build fallow Images:    <<<<====    "
cat images_build
